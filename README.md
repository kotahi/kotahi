# Kotahi

[Website](https://kotahi.community/)  
Join our [chat channel](https://mattermost.coko.foundation/coko/channels/kotahi).

Kotahi is a modern submission, review and publishing system especially suited for academic journals, overlay journals, preprint curation groups, data publications, data review, research labs and PRC (publish-review-curate) groups. Kotahi supports:

- individual **research object submission**
- **bulk imports** of preprints and other documents/data objects for review
- **highly customisable** data and workflows
- **single source**, so that authors, editors, reviewers and production editors work on a single source without needing to send versions back and forth
- **automated export** to various formats including **JATS**, HTML, PDF
- integrations with DOAJ, Crossref, DataCite and more
- a built in, highly customisable **publishing site and CMS** (content management system)
- several **other publishing options** including **DOI registration**
- automated **citation management and structuring tools** via API lookups 
- a **task management** system to guide team-members, plus **in-app chat** and alerts to simplify communication
- **multitenancy**, allowing multiple groups to be hosted in a single instance, and allowing easy experimentation with new workflows
- a **plugin architecture** for custom functionality and easy extensibility
- modern interfaces, architecture, thinking


Kotahi is currently under development by the [Kotahi Foundation](https://kotahi.foundation/).
 
## Project roadmap

[The Kotahi product roadmap](https://miro.com/app/board/uXjVLmKGfek=/) can be viewed on Miro.

## Bug reporting

To report a bug, [open a GitLab issue](https://gitlab.coko.foundation/kotahi/kotahi/-/issues/new) and use the bug-report template contained in the issue.

## Deploying kotahi

You can find our deployment documentation [here](https://kotahi-dev-docs.fly.dev/docs/deployment/Kotahi%20deployment%20guide).

## Running kotahi for development

You can find development documentation [here](https://kotahi-dev-docs.fly.dev/docs/development/Getting%20started).  
Documentation for underlying coko libraries can be found [here](https://coko-dev-docs.fly.dev/).

## Further info

See [FAQ.md](FAQ.md).  
Conventions for code changes, branching and merge requests are covered in [CONTRIBUTING.md](CONTRIBUTING.md).

