# This file is meant for running a production environment locally

services:
  client:
    build:
      context: ./packages/client
      dockerfile: ./Dockerfile-production
    ports:
      - ${CLIENT_PORT:-4000}:4000
    environment:
      - SERVER_URL=${SERVER_URL}
      - YJS_WEBSOCKET_SERVER_URL=${YJS_WEBSOCKET_SERVER_URL}

  server:
    build:
      context: ./packages/server
      dockerfile: ./Dockerfile-production
    depends_on:
      - db
      - fileHosting
    ports:
      - ${SERVER_PORT:-3000}:${SERVER_PORT:-3000}
      - ${WS_YJS_SERVER_PORT:-5010}:${WS_YJS_SERVER_PORT:-5010}
    environment:
      - NODE_ENV=production
      - POSTGRES_USER=${POSTGRES_USER:-dev}
      - POSTGRES_PASSWORD=${POSTGRES_PASSWORD:-password}
      - POSTGRES_DB=${POSTGRES_DB:-kotahi_dev}
      - POSTGRES_HOST=db
      - POSTGRES_PORT=5432
      - PUBSWEET_SECRET=${PUBSWEET_SECRET:-theSecret}
      - SERVER_PORT=${SERVER_PORT}
      - CLIENT_URL=http://localhost:4000
      - SERVER_URL=http://127.0.0.1:3000
      - ORCID_CLIENT_ID=${ORCID_CLIENT_ID}
      - ORCID_CLIENT_SECRET=${ORCID_CLIENT_SECRET}
      - USE_SANDBOXED_ORCID=${USE_SANDBOXED_ORCID:-false}
      - USE_COLAB_EMAIL=${USE_COLAB_EMAIL:-false}
      - KOTAHI_API_TOKENS=${KOTAHI_API_TOKENS:-}
      - JOURNAL_NAME=${JOURNAL_NAME:-}
      - JOURNAL_ABBREVIATED_NAME=${JOURNAL_ABBREVIATED_NAME:-}
      - JOURNAL_HOMEPAGE=${JOURNAL_HOMEPAGE:-}
      - GOOGLE_SPREADSHEET_CLIENT_EMAIL=${GOOGLE_SPREADSHEET_CLIENT_EMAIL:-}
      - GOOGLE_SPREADSHEET_PRIVATE_KEY=${GOOGLE_SPREADSHEET_PRIVATE_KEY:-}
      - GOOGLE_SPREADSHEET_ID=${GOOGLE_SPREADSHEET_ID:-}
      - HYPOTHESIS_API_KEY=${HYPOTHESIS_API_KEY:-}
      - HYPOTHESIS_GROUP=${HYPOTHESIS_GROUP:-}
      - HYPOTHESIS_ALLOW_TAGGING=${HYPOTHESIS_ALLOW_TAGGING}
      - HYPOTHESIS_REVERSE_FIELD_ORDER=${HYPOTHESIS_REVERSE_FIELD_ORDER}
      - CROSSREF_LOGIN=${CROSSREF_LOGIN:-}
      - CROSSREF_PASSWORD=${CROSSREF_PASSWORD:-}
      - CROSSREF_REGISTRANT=${CROSSREF_REGISTRANT:-}
      - CROSSREF_DEPOSITOR_NAME=${CROSSREF_DEPOSITOR_NAME:-}
      - CROSSREF_DEPOSITOR_EMAIL=${CROSSREF_DEPOSITOR_EMAIL:-}
      - CROSSREF_PUBLICATION_TYPE=${CROSSREF_PUBLICATION_TYPE:-article}
      - CROSSREF_USE_SANDBOX=${CROSSREF_USE_SANDBOX:-false}
      - DOI_PREFIX=${DOI_PREFIX:-}
      - PUBLISHED_ARTICLE_LOCATION_PREFIX=${PUBLISHED_ARTICLE_LOCATION_PREFIX:-}
      - PUBLICATION_LICENSE_URL=${PUBLICATION_LICENSE_URL:-}
      - MANUSCRIPTS_TABLE_COLUMNS=${MANUSCRIPTS_TABLE_COLUMNS}
      - DISPLAY_SHORTID_AS_IDENTIFIER=${DISPLAY_SHORTID_AS_IDENTIFIER:-false}
      - GMAIL_NOTIFICATION_EMAIL_AUTH=${GMAIL_NOTIFICATION_EMAIL_AUTH:-}
      - GMAIL_NOTIFICATION_EMAIL_SENDER_NAME=${GMAIL_NOTIFICATION_EMAIL_SENDER_NAME:-}
      - GMAIL_NOTIFICATION_PASSWORD=${GMAIL_NOTIFICATION_PASSWORD:-}
      - PUBLISHING_WEBHOOK_URL=${PUBLISHING_WEBHOOK_URL:-}
      - PUBLISHING_WEBHOOK_TOKEN=${PUBLISHING_WEBHOOK_TOKEN:-}
      - PUBLISHING_WEBHOOK_REF=${PUBLISHING_WEBHOOK_REF:-}
      - REVIEW_SHARED=${REVIEW_SHARED:-false}
      - REVIEW_HIDE=${REVIEW_HIDE:-false}
      - NOTIFICATION_EMAIL_AUTOMATED=${NOTIFICATION_EMAIL_AUTOMATED:-false}
      - NOTIFICATION_EMAIL_CC_ENABLED=${NOTIFICATION_EMAIL_CC_ENABLED:-false}
      - SERVICE_PAGEDJS_CLIENT_ID=${SERVICE_PAGEDJS_CLIENT_ID}
      - SERVICE_PAGEDJS_SECRET=${SERVICE_PAGEDJS_SECRET}
      - SERVICE_PAGEDJS_PROTOCOL=${SERVICE_PAGEDJS_PROTOCOL}
      - SERVICE_PAGEDJS_HOST=${SERVICE_PAGEDJS_HOST}
      - SERVICE_PAGEDJS_PORT=${SERVICE_PAGEDJS_PORT}
      - SERVICE_ANYSTYLE_CLIENT_ID=${SERVICE_ANYSTYLE_CLIENT_ID}
      - SERVICE_ANYSTYLE_SECRET=${SERVICE_ANYSTYLE_SECRET}
      - SERVICE_ANYSTYLE_PROTOCOL=${SERVICE_ANYSTYLE_PROTOCOL}
      - SERVICE_ANYSTYLE_HOST=${SERVICE_ANYSTYLE_HOST}
      - SERVICE_ANYSTYLE_PORT=${SERVICE_ANYSTYLE_PORT}
      - S3_URL=${S3_URL:-http://fileHosting:9000}
      - S3_ACCESS_KEY_ID=nonRootUser
      - S3_SECRET_ACCESS_KEY=nonRootPassword
      - S3_BUCKET=uploads
      - S3_REGION=${S3_REGION}
      - AUTO_IMPORT_HOUR_UTC=${AUTO_IMPORT_HOUR_UTC:-}
      - DISABLE_EVENT_NOTIFICATIONS=${DISABLE_EVENT_NOTIFICATIONS:-}
      - ARCHIVE_PERIOD_DAYS=${ARCHIVE_PERIOD_DAYS:-}
      - ALLOW_MANUAL_IMPORT=${ALLOW_MANUAL_IMPORT:-}
      - SEMANTIC_SCHOLAR_IMPORTS_RECENCY_PERIOD_DAYS=${SEMANTIC_SCHOLAR_IMPORTS_RECENCY_PERIOD_DAYS:-}
      - TEAM_TIMEZONE=${TEAM_TIMEZONE}
      - SERVICE_XSWEET_CLIENT_ID=${SERVICE_XSWEET_CLIENT_ID}
      - SERVICE_XSWEET_SECRET=${SERVICE_XSWEET_SECRET}
      - SERVICE_XSWEET_PROTOCOL=${SERVICE_XSWEET_PROTOCOL}
      - SERVICE_XSWEET_HOST=${SERVICE_XSWEET_HOST}
      - SERVICE_XSWEET_PORT=${SERVICE_XSWEET_PORT}
      - FLAX_EXPRESS_PORT=${FLAX_EXPRESS_PORT}
      - FLAX_EXPRESS_HOST=${FLAX_EXPRESS_HOST:-kotahi-flax-site}
      - FLAX_EXPRESS_PROTOCOL=${FLAX_EXPRESS_PROTOCOL:-http}
      - FLAX_CLIENT_ID=${FLAX_CLIENT_ID}
      - FLAX_CLIENT_SECRET=${FLAX_CLIENT_SECRET}
      - FLAX_CLIENT_API_URL=${FLAX_CLIENT_API_URL}
      - FLAX_SITE_URL=${FLAX_SITE_URL}
      - USE_APERTURE_EMAIL=${USE_APERTURE_EMAIL}
      - INSTANCE_GROUPS=${INSTANCE_GROUPS:-kotahi:journal}
      - USE_COLAB_BIOPHYSICS_IMPORT=${USE_COLAB_BIOPHYSICS_IMPORT:-false}
      - E2E_TESTING_API=${E2E_TESTING_API}
      - WS_YJS_SERVER_PORT=${WS_YJS_SERVER_PORT:-5010}
    volumes:
      - ./packages/client/config/translation:/home/node/app/config/translation
    healthcheck:
      test:
        ['CMD-SHELL', 'curl --fail http://localhost:3000/healthcheck || exit 1']
      start_period: 30s # Give the server seconds to start before healthchecks begin
      timeout: 5s
      retries: 5

  pagedjs:
    depends_on:
      - db-pagedjs
    image: cokoapps/pagedjs:2.0.6
    ports:
      - ${SERVICE_PAGEDJS_PORT:-3003}:${SERVICE_PAGEDJS_PORT:-3003}
    environment:
      - NODE_ENV=development
      - SECRET=pagedjs_dev_secret
      - SERVER_PORT=${SERVICE_PAGEDJS_PORT:-3003}
      - SERVER_URL=http://localhost:${SERVICE_PAGEDJS_PORT:-3003}
      - POSTGRES_HOST=db-pagedjs
      - POSTGRES_PORT=5432
      - POSTGRES_DB=pagedjs_dev
      - POSTGRES_USER=pagedjs_user_dev
      - POSTGRES_PASSWORD=pagedjs_user_password
    security_opt:
      - seccomp:unconfined

  db-pagedjs:
    image: postgres:16-alpine
    environment:
      - POSTGRES_USER=pagedjs_user_dev
      - POSTGRES_DB=pagedjs_dev
      - POSTGRES_PASSWORD=pagedjs_user_password

  xsweet:
    depends_on:
      - db-xsweet
    image: cokoapps/xsweet:3.0.2
    ports:
      - ${SERVICE_XSWEET_PORT:-3004}:${SERVICE_XSWEET_PORT:-3004}
    environment:
      - NODE_ENV=development
      - SECRET=xsweet_dev_secret
      - SERVER_PORT=${SERVICE_XSWEET_PORT:-3004}
      - POSTGRES_HOST=db-xsweet
      - POSTGRES_PORT=5432
      - POSTGRES_DB=xsweet_dev
      - POSTGRES_USER=xsweet_user_dev
      - POSTGRES_PASSWORD=xsweet_user_password

  db-xsweet:
    image: postgres:16-alpine
    environment:
      - POSTGRES_DB=xsweet_dev
      - POSTGRES_USER=xsweet_user_dev
      - POSTGRES_PASSWORD=xsweet_user_password

  db:
    image: postgres:16-alpine
    ports:
      - ${POSTGRES_PORT:-5432}:5432
    environment:
      - POSTGRES_DB=${POSTGRES_DB:-kotahi_dev}
      - POSTGRES_USER=${POSTGRES_USER:-dev}
      - POSTGRES_PASSWORD=${POSTGRES_PASSWORD:-password}

  fileHosting:
    image: minio/minio:RELEASE.2023-05-04T21-44-30Z
    ports:
      - ${S3_PORT:-9000}:9000
      - ${MINIO_CONSOLE_PORT:-9001}:9001
    # volumes:
    #   - minio_storage:/data
    environment:
      - MINIO_ROOT_USER=admin
      - MINIO_ROOT_PASSWORD=superSecretAdminPassword
    command: server --console-address ":9001" /data
    healthcheck:
      test: ['CMD', 'curl', '-f', 'http://localhost:9000/minio/health/live']
      interval: 0.5s
      timeout: 20s
      retries: 30

  createbucket:
    image: minio/mc:RELEASE.2023-05-04T18-10-16Z
    depends_on:
      fileHosting:
        condition: service_healthy
    entrypoint: >
      /bin/sh -c "
      /usr/bin/mc config host add cokoServer http://fileHosting:9000 admin superSecretAdminPassword;
      /usr/bin/mc admin user add cokoServer/ nonRootUser nonRootPassword;
      /usr/bin/mc admin user enable cokoServer/ nonRootUser;
      /usr/bin/mc mb cokoServer/uploads;
      /usr/bin/mc admin policy attach cokoServer/ readwrite --user=nonRootUser;
      exit 0;
      "

  anystyle:
    depends_on:
      - db-anystyle
    image: cokoapps/anystyle:2.0.2
    ports:
      - ${SERVICE_ANYSTYLE_PORT:-4567}:${SERVICE_ANYSTYLE_PORT:-4567}
    environment:
      - NODE_ENV=development
      - SECRET=anystyle_dev_secret
      - SERVER_PORT=${SERVICE_ANYSTYLE_PORT:-4567}
      - POSTGRES_HOST=db-anystyle
      - POSTGRES_PORT=5432
      - POSTGRES_DB=anystyle_dev
      - POSTGRES_USER=anystyle_user_dev
      - POSTGRES_PASSWORD=anystyle_user_password

  db-anystyle:
    image: postgres:16-alpine
    environment:
      - POSTGRES_USER=anystyle_user_dev
      - POSTGRES_DB=anystyle_dev
      - POSTGRES_PASSWORD=anystyle_user_password

  kotahi-flax-site:
    image: cokoapps/kotahi-flax:0.5.3
    depends_on:
      server:
        condition: service_healthy
    ports:
      - ${FLAX_EXPRESS_PORT:-8082}:${FLAX_EXPRESS_PORT:-3000}
      - ${FLAX_SITE_PORT:-8081}:80
    environment:
      - FLAX_EXPRESS_PORT=${FLAX_EXPRESS_PORT:-3000}
      - FLAX_CLIENT_ID=${FLAX_CLIENT_ID}
      - FLAX_CLIENT_SECRET=${FLAX_CLIENT_SECRET}
      - FLAX_CLIENT_API_URL=${FLAX_CLIENT_API_URL}

  devdocs:
    build:
      context: ./packages/devdocs
      dockerfile: ./Dockerfile-production
    ports:
      - ${DEV_DOCS_PORT:-4001}:80
