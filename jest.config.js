module.exports = {
  projects: [
    {
      displayName: 'client',
      testEnvironment: 'jsdom',
      // verbose: true,
      testURL: 'http://localhost/',
      testMatch: ['**/packages/client/**/__tests__/**.test.js'],
      setupFilesAfterEnv: [require.resolve('regenerator-runtime/runtime')],
    },
    {
      displayName: 'server',
      testEnvironment: 'node',
      // verbose: true,
      testMatch: ['**/packages/server/**/__tests__/**.test.js'],
      setupFilesAfterEnv: [require.resolve('regenerator-runtime/runtime')],
    },
  ],
}
