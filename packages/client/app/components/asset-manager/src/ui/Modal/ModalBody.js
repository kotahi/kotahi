import styled from 'styled-components'

import { th } from '@coko/client'

const Body = styled.div`
  flex-grow: 1;
  font-family: ${th('fontInterface')};
  height: 100%;
  overflow: auto;
`

export default Body
