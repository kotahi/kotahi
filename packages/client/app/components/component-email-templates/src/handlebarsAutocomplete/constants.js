import GlobalState from '../../../../utils/GlobalState'

export const handlebars = GlobalState.get('handlebars')
export const DROPDOWN_ID = 'handlebars-autocomplete'
export const BRACKETS_TYPES = {
  text: 2,
  link: 3,
}
