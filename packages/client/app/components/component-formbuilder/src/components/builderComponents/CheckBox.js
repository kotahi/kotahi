import React from 'react'
import { CheckboxGroup } from '../../../../pubsweet'

const CheckboxFieldBuilder = input => <CheckboxGroup {...input} />
export default CheckboxFieldBuilder
