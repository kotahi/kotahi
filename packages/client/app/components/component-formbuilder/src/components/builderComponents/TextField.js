import React from 'react'
import { TextField } from '../../../../pubsweet'

const TextFieldBuilder = input => <TextField {...input} />
export default TextFieldBuilder
