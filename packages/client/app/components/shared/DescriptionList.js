import styled from 'styled-components'
import { th, grid } from '@coko/client'

// eslint-disable-next-line import/prefer-default-export
export const DescriptionList = styled.div`
  background-color: ${th('colorBackground')};
  padding: ${grid(2)} ${grid(3)};
`
