import FullWaxEditor from './FullWaxEditor'
import ProductionWaxEditor from './ProductionWaxEditor'
import SimpleWaxEditor from './SimpleWaxEditor'

export default {
  FullWaxEditor,
  ProductionWaxEditor,
  SimpleWaxEditor,
}
