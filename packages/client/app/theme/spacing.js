// TODO : Remove after replacing throghout kotahi

const spacing = {
  /** 1px */
  a: '1px',
  /** 2px */
  b: '2px',
  /** 3px */
  c: '3px',
  /** 5px */
  d: '5px',
  /** 7.5px */
  e: '7.5px',
  /** 15px */
  f: '15px',
  /** 30px */
  g: '30px',
  /** 45px */
  h: '45px',
  /** 60px */
  i: '60px',
  /** 90px */
  j: '90px',
  /** 135px */
  k: '135px',
}

export default spacing
