module.exports = {
  testEnvironment: 'jsdom',
  testMatch: ['**/__tests__/**.test.js'],
  transform: {
    '^.+\\.[tj]sx?$': 'babel-jest',
  },
}
