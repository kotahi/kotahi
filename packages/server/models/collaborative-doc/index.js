const model = require('./collaborativeDoc.model')

module.exports = {
  model,
  modelName: 'CollaborativeDoc',
}
