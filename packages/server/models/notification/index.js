const model = require('./notification.model')

module.exports = {
  model,
  modelName: 'Notification',
}
