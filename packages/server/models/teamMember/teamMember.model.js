const { TeamMember: TeamMemberBase } = require('@coko/server')

// REFACTOR: MODELS
const { evictFromCacheByPrefix } = require('../../server/querycache')

class TeamMember extends TeamMemberBase {
  static get modifiers() {
    return {
      orderByCreatedDesc(builder) {
        builder.orderBy('created', 'desc')
      },
    }
  }

  // TODO add $beforeDelete once https://gitlab.coko.foundation/cokoapps/server/-/issues/43 is resolved
  async $beforeInsert(queryContext) {
    await super.$beforeInsert(queryContext)
    evictFromCacheByPrefix('userIs')
    evictFromCacheByPrefix('membersOfTeam')
  }

  static get schema() {
    return {
      properties: {
        isShared: { type: ['boolean', 'null'] },
      },
    }
  }
}

module.exports = TeamMember
